create table books (
       id  int not null primary key auto_increment,
        title varchar(30),
        author varchar(30),
        publisher varchar(20),
        year_pub int,
        description varchar(100)
);