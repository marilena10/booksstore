create table user_table
(
    id  int not null primary key auto_increment,
    login    varchar(50),
    password varchar(500),
    role_id  varchar(50) DEFAULT 'ROLE_ADMIN'
);
create unique index user_table_login_uindex
    on user_table (login);

insert into user_table (login, password) values('marilena', 'pass');