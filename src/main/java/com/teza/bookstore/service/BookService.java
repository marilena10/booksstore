package com.teza.bookstore.service;

import com.teza.bookstore.model.Book;
import com.teza.bookstore.respository.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BookService {

    @Autowired
    private final BookRepository bookRepository;

    public List getAllBooks() {
        return bookRepository.findAll();
    }

    public Book getBookById(Integer id) {
        return bookRepository.findById(id).get();
    }

    public void addBook(Book book) {
        bookRepository.save(book);
    }

    public void updateBook(Integer id, Book book) {
        Book book1 = bookRepository.findById(id).get();
        book1.setTitle(book.getTitle());
        book1.setAuthor(book.getAuthor());
        book1.setYear(book.getYear());
        book1.setPublisher(book.getPublisher());
        book1.setDescription(book.getDescription());
    }

    public void deleteBook(Integer id) {
        bookRepository.deleteById(id);
    }

    public Book findByTitle(String title) {
        return bookRepository.findByTitle(title);
    }

    public List<Book> search(String keyword) {
        return bookRepository.search(keyword);
    }
}
