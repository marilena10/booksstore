package com.teza.bookstore.controller;

import com.teza.bookstore.model.Book;
import com.teza.bookstore.service.BookService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class BookController {

    @Autowired
    BookService bookService;

    @PostMapping("/admin/saveBook")
    public String addBookAdmin(@Valid @ModelAttribute("newBook") Book book) {
        bookService.addBook(book);
        return "redirect:/admin";
    }

    @GetMapping("/books")
    public String getAllBooks(Model model) {
        List<Book> listBook = bookService.getAllBooks();
        model.addAttribute("listBook", listBook);
        return "index";
    }

    @GetMapping("/books/{id}")
    public Book getBookById(@PathVariable Integer id) {
        return bookService.getBookById(id);
    }

    @GetMapping("/update/{id}")
    public String updatePage(@PathVariable Integer id, Model model) {
        Book book = bookService.getBookById(id);
        model.addAttribute("book", book);
        return "update";
    }

    @PostMapping("/admin/update/{id}")
    public String updateBookAdmin(@Valid Book book, @PathVariable Integer id, BindingResult result, Model model) {
        if (result.hasErrors()) {
            book.setId(id);
            return "update";
        }
        bookService.updateBook(id, book);
        return "redirect:/admin";
    }

    @GetMapping("/admin/delete/{id}")
    public String deleteBookAdmin(@PathVariable Integer id, Model model) {
        bookService.deleteBook(id);
        return "redirect:/admin";
    }

    @GetMapping("/books/search")
    public String search(@RequestParam("keyword") String keyword, Model model) {
        List<Book> listOfSearch = bookService.search(keyword);
        model.addAttribute("listBook", listOfSearch);
        model.addAttribute("keyword", keyword);
        model.addAttribute("pageTitle", "Search result for " + keyword);
        return "search_result";
    }

    @GetMapping("/admin/search")
    public String adminSearch(@RequestParam("keyword") String keyword, Model model) {
        List<Book> listOfSearch = bookService.search(keyword);
        model.addAttribute("listBook", listOfSearch);
        model.addAttribute("keyword", keyword);
        model.addAttribute("pageTitle", "Search result for " + keyword);
        return "search_result_admin";
    }

    @GetMapping("/admin")
    public String adminPageAllBooks(Model model) {
        System.out.println();
        List<Book> listBook = bookService.getAllBooks();
        model.addAttribute("listBook", listBook);
        return "admin";
    }

    @GetMapping("/admin/create")
    public String createBookPage(Model model) {
        Book book = new Book();
        model.addAttribute("newBook", book);
        return "create";
    }
}
