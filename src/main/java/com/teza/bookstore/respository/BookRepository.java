package com.teza.bookstore.respository;

import com.teza.bookstore.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {
    @Transactional
    Book findByTitle(String title);

    @Transactional
    @Query(value = "Select * from books where match (title, author, publisher, description) "
            + "against (?1)", nativeQuery = true)
    List<Book> search(String keywork);
}
