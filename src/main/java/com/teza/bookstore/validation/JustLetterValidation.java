package com.teza.bookstore.validation;


import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class JustLetterValidation implements ConstraintValidator<JustLetter, String> {

    private static Pattern justLetterPattern;

    @Override
    public void initialize(JustLetter constraintAnnotation) {
        if (justLetterPattern == null) {
            justLetterPattern = Pattern.compile("(^[A-Z]{1}[a-z]+\\s?[A-Z]*[a-z]*$)");
        }
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        boolean result = false;

        if (isNotNull(s)) {
            result = matchPostCode(s);
        }

        return result;
    }


    private boolean matchPostCode(String s) {

        Matcher matcher = justLetterPattern.matcher(s);
        return matcher.matches();
    }

    private boolean isNotNull(String author) {
        return author != null;
    }

}
