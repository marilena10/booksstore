package com.teza.bookstore.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = JustLetterValidation.class)
public @interface JustLetter {

    String message() default "{just.letter.constraint}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
