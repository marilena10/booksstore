package com.teza.bookstore.validation;

import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Calendar;

@Component
public class YearValidation implements ConstraintValidator<Year, Integer> {

    @Override
    public void initialize(Year constraintAnnotation) {
    }

    @Override
    public boolean isValid(Integer book, ConstraintValidatorContext constraintValidatorContext) {
        boolean result = false;
        result = book <= Calendar.getInstance().get(Calendar.YEAR);
        return result;
    }

}
