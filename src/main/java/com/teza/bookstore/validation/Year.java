package com.teza.bookstore.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = YearValidation.class)
public @interface Year {
    String message() default "{year.constraint}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
