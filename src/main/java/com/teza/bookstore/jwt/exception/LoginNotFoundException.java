package com.teza.bookstore.jwt.exception;

import org.springframework.security.core.AuthenticationException;

public class LoginNotFoundException extends AuthenticationException {
    public LoginNotFoundException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public LoginNotFoundException(String msg) {
        super(msg);
    }
}