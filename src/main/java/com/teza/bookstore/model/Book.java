package com.teza.bookstore.model;

import com.teza.bookstore.validation.JustLetter;
import com.teza.bookstore.validation.Year;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "books")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Book {
    @Id
    @GenericGenerator(name = "kaugen", strategy = "increment")
    @GeneratedValue(generator = "kaugen")
    private Integer id;
    @Size(max = 50, min = 3, message = "{title.name.invalid}")
    private String title;
    @Size(max = 30, min = 5, message = "{author.name.invalid}")
    //@Pattern(regexp = "(^[A-Z]{1}[a-z]+(\\s[A-Z]){1}[a-z]+$)")
    @JustLetter
    private String author;
    @Size(max = 15, min = 3, message = "{publisher.name.invalid}")
    private String publisher;
    @Column(name = "year_pub")
    @NotNull
    @Year
    private Integer year;
    @Size(max = 100, min = 3, message = "{description.is.invalid}")
    private String description;
}
