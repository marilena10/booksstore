package com.teza.bookstore.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class BookValidationTest {
    private static Validator validator;
    private Book book = new Book();

    @BeforeEach
    public void setUpClass() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @BeforeEach
    public void setUp() {
        book.setId(3);
        book.setTitle("Gold NINE");
        book.setAuthor("Hjghgj");
        book.setPublisher("litera");
        book.setDescription("bkaasghgsa");
    }

    @Test
    public void authorHaveNumbers_constraintViolation() {
        book.setYear(2020);
        Set<ConstraintViolation<Book>> constraintViolations = validator.validate(book);

        assertEquals(1, constraintViolations.size());

        //printViolations(constraintViolations);
    }

    private void printViolations(Set<ConstraintViolation<Book>> constraintViolations) {
        for (ConstraintViolation<Book> violation : constraintViolations) {

            String invalidValue = (String) violation.getInvalidValue();
            String message = violation.getMessage();
            System.out.println("Found constraint violation. Value: " + invalidValue
                    + " Message: " + message);
        }
    }


}