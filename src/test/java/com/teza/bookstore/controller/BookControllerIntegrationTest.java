package com.teza.bookstore.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.teza.bookstore.model.Book;
import com.teza.bookstore.service.BookService;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class BookControllerIntegrationTest {
    private final Gson gson = new GsonBuilder().serializeNulls().create();
    private final int id = 1;
    Book bookInit;
    List<Book> bookList = new ArrayList();
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private BookService bookService;

    @BeforeEach
    void setUp() {
        bookInit = Book.builder()

                .title("Practica")
                .author("Marilena")
                .publisher("usm")
                .year(2021)
                .description("raport de practica")
                .build();
    }

    @Test
    void addBook() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/books")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(bookInit)))
                .andExpect(status().isOk())
                .andReturn();
        bookInit.setId(bookService.findByTitle(bookInit.getTitle()).getId());
        bookList.add(bookInit);
        Assert.assertEquals(bookService.getAllBooks(), bookList);
    }

    @Test
    void getAllBooks() throws Exception {
        Assert.assertEquals(mockMvc.perform(MockMvcRequestBuilders.get("/books")
                        .contentType(MediaType.APPLICATION_JSON))
                        .andReturn()
                        .getResponse()
                        .getContentAsString()
                        .replaceAll("\\s", ""),
                bookService.getAllBooks().stream()
                        .map(gson::toJson)
                        .collect(Collectors.toList())
                        .toString()
                        .replaceAll("\\s", ""));
    }

    @Test
    void getBookById() throws Exception {
        Assert.assertEquals(mockMvc.perform(MockMvcRequestBuilders.get("/books/" + id)
                        .contentType(MediaType.APPLICATION_JSON))
                        .andReturn()
                        .getResponse()
                        .getContentAsString(),
                gson.toJson(bookService.getBookById(id)));
    }

    @Test
    void updateBook() {
    }

    @Test
    void deleteBook() {
    }
}