package com.teza.bookstore.service;

import com.teza.bookstore.model.Book;
import com.teza.bookstore.respository.BookRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BookServiceTest {
    @Autowired
    BookService bookService;
    @MockBean
    BookRepository bookRepository;

    Book book;
    List<Book> bookList = new ArrayList();

    @BeforeAll
    public void setUp() {
        book = Book.builder()
                .title("Practica")
                .author("Marilena")
                .publisher("usm")
                .year(2021)
                .description("raport de practica")
                .build();
        bookList.add(book);
    }

    @Test
    void getAllBooks_shouldGetTheCreatedBook() {
        Mockito.when(bookRepository.findAll()).thenReturn(bookList);
        assertEquals(bookService.getAllBooks(), bookList);
    }

    @Test
    void getBookById() {
        Mockito.when(bookRepository.findById(1)).thenReturn(java.util.Optional.ofNullable(book));
        assertEquals(bookService.getBookById(1), book);
    }

    @Test
    void addBook() {
        Mockito.doNothing().when(bookRepository.save(Mockito.any()));
        bookService.addBook(Mockito.any());
        Mockito.verify(bookService, Mockito.times(1)).addBook(Mockito.any());
    }

    @Test
    void updateBook() {
    }

    @Test
    void deleteBook() {
    }

    @Test
    void findByTitle() {
    }
}